<?php
defined('BASEPATH') OR exit('no direct script allowed');

$lang['hello'] = 'Bonjour';
$lang['query'] = 'Question';
$lang['web'] = 'Toile';
$lang['image'] = 'Image';
$lang['video'] = 'Video';
$lang['news'] = 'Nouvelles';
$lang['google'] = 'Google';
$lang['maps'] = 'Cartes';
$lang['search'] = 'Recherche';
$lang['result'] = 'About Result';
$lang['save'] = 'Save';