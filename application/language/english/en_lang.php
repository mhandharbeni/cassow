<?php
defined('BASEPATH') OR exit('no direct script allowed');

$lang['hello'] = 'hello';
$lang['query'] = 'Query';
$lang['web'] = 'Web';
$lang['image'] = 'Image';
$lang['video'] = 'Video';
$lang['news'] = 'News';
$lang['google'] = 'Google';
$lang['maps'] = 'Maps';
$lang['search'] = 'Search';
$lang['result'] = 'About Result';
$lang['save'] = 'Save';