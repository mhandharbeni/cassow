<?php
defined('BASEPATH') OR exit('no direct script allowed');

$lang['hello'] = 'hallo';
$lang['query'] = 'Pertanyaan';
$lang['web'] = 'Jaringan';
$lang['image'] = 'Gambar';
$lang['video'] = 'Video';
$lang['news'] = 'Berita';
$lang['google'] = 'Google';
$lang['maps'] = 'Peta';
$lang['search'] = 'Pencarian';
$lang['result'] = 'Hasil Pencarian';
$lang['save'] = 'Simpan';