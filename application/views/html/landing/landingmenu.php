        <div class="wrapper">
            <div class="right-menu">
                <button type="button" class="btn-togggle dropdown-toggle" data-toggle="dropdown">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="<?=base_url('Search/about')?>">What is Search?</a></li>
                    <li><a href="<?=base_url('Search/addtobrowser')?>">Add to Browser</a></li>
                    <li><a href="<?=base_url('Search/blog')?>">Blog</a></li>
                    <li><a href="<?=base_url('Search/help')?>">Help</a></li>
                    <li><a href="<?=base_url('Search/contact')?>">Contact</a></li>
                    <li><a href="<?=base_url('Search/setting')?>">Advanced Settings</a></li>
                    <li class="divider"></li>
                    <li class="select-theme">
                        <div class="select-theme-label">Select Theme</div>
                        <a href="<?=base_url('Search/setDefaultTheme/2')?>"><img src="<?=base_url('assets/style/blue/img/egg-black.png')?>"></a>
                        <a href="<?=base_url('Search/setDefaultTheme/1')?>"><img src="<?=base_url('assets/style/blue/img/egg-blue.png')?>"></a>
                    </li>
                </ul>
            </div>
