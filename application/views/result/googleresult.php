    <style type='text/css'>
        .gsc-orderby{
          display: none;
        }
        /* Use a different font family for search results */
        .gs-title, .gs-snippet {
            font-family: courier;
        }
        
        /* Add a border between individual search results */
        .gs-webResult {
            border: 0px;
            padding: 1em;
        }
        
        /* Do no display the count of search results */    
        .gsc-result-info {
            display: none;
        }
        
        /* Hide the Google branding in search results */
        .gcsc-branding {
            display: none; 
        }
        
        /* Hide the thumbnail images in search results */
        .gsc-thumbnail {
            display: none;
        }
        
        /* Hide the snippets in Google search results */
        .gs-snippet { 
            display: block;
        }
        
        /* Change the font size of the title of search results */
        .gs-title a { 
            font-size: 16px;  
        }
        
        /* Change the font size of snippets inside search results */
        .gs-snippet {
            font-size: 14px;
        }
        
        /* Google Custom Search highlights matching words in bold, toggle that */
        .gs-title b, .gs-snippet b {
            font-weight: normal;
        }
        
        /* Do no display the URL of web pages in search results */
        .gsc-url-top, .gsc-url-bottom {
            display: none;
        }
        //url bottom
        .gsc-url-bottom {
            display: block;
        }
        /* Highlight the pagination buttons at the bottom of search results */
        //styling paging
        .gsc-cursor-page {
            font-size: 1.5em;
            padding: 4px 8px;
            border: 0px solid #ccc;
        }
        
    </style>
<script>
  (function() {
    var cx = '004603905213444256273:nozyqsc5k0m';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchresults-only></gcse:searchresults-only>